package pl.pich.finances.bill.dto.bill;

import lombok.Data;

@Data
public class StatisticsDto {
    private Boolean payed;
}

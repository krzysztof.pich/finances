package pl.pich.finances.bill.dto;

import lombok.Data;
import org.springframework.lang.Nullable;
import pl.pich.finances.bill.dto.bill.StatisticsDto;
import pl.pich.finances.bill.model.Period;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class BillDto {
    private Integer id;
    private String name;
    private BigDecimal amount;
    private Integer timeOfPayment;
    private Period period;
    private Integer intervalModulo;
    private Date startDate;
    private Date endDate;
    private StatisticsDto statistics;
    @Nullable
    private Boolean payed;
}

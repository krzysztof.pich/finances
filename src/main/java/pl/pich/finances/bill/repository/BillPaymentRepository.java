package pl.pich.finances.bill.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pich.finances.bill.model.Payment;

public interface BillPaymentRepository extends CrudRepository<Payment, Integer> {
}

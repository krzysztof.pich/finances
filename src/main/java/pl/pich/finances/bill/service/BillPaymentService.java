package pl.pich.finances.bill.service;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import pl.pich.finances.app.exceptions.NotFoundException;
import pl.pich.finances.bill.dto.BillPaymentDto;
import pl.pich.finances.bill.model.Bill;
import pl.pich.finances.bill.model.Payment;
import pl.pich.finances.bill.repository.BillPaymentRepository;
import pl.pich.finances.user.model.User;

@Service
public class BillPaymentService {
    private final BillService billService;
    private final BillPaymentRepository paymentRepository;
    private final ModelMapper modelMapper;

    public BillPaymentService(BillService billService, BillPaymentRepository paymentRepository, ModelMapper modelMapper) {
        this.billService = billService;
        this.paymentRepository = paymentRepository;
        this.modelMapper = modelMapper;
    }


    public BillPaymentDto addPayment(Integer billId, User user, BillPaymentDto payment) throws NotFoundException {
        Bill bill = billService.getBill(user, billId);
        Payment paymentModel = this.modelMapper.map(payment, Payment.class);
        paymentModel.setBill(bill);
        paymentRepository.save(paymentModel);
        payment.setId(paymentModel.getId());
        return payment;
    }

}
